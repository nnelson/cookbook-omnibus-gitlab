# Cookbook Name:: omnibus-gitlab
# Recipe:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'
require 'chef-vault'

describe 'omnibus-gitlab::default' do
  context 'with basic gitlab.rb' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
        node.normal['omnibus-gitlab']['gitlab_rb']['external_url'] = 'http://herpderp-extern.com'
        node.normal['omnibus-gitlab']['gitlab_rb']['pages_external_url'] = 'http://herpderp-pages.com'
        node.normal['omnibus-gitlab']['gitlab_rb']['logging']['udp_log_shipping_host'] = '127.0.0.1'
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.not_to raise_error
    end

    it 'creates gitlab.rb' do
      expect(chef_run).to create_template('/etc/gitlab/gitlab.rb').with(
        mode: '0600'
      )
    end

    it 'sets the logging name' do
      expect(chef_run).to render_file('/etc/gitlab/gitlab.rb').with_content { |c|
        expect(c).to include("external_url 'http://herpderp-extern.com'")
        expect(c).to include("pages_external_url 'http://herpderp-pages.com'")
        expect(c).to include("logging['udp_log_shipping_hostname'] = 'fauxhai.local'")
      }
    end

    context 'when `skip_auto_reconfigure` is set' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['skip_auto_reconfigure'] = true
        end.converge(described_recipe)
      end

      it 'creates the skip file' do
        expect(chef_run).to create_file('/etc/gitlab/skip-auto-reconfigure')
      end
    end

    context 'when package install is enabled' do
      before do
        allow(Dir).to receive(:exist?).with('/somedir').and_return(true)
      end

      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['package']['name'] = "gitlab-ee"
          node.normal['omnibus-gitlab']['package']['version'] = "11.9.8-ee.0"
          node.normal['omnibus-gitlab']['package']['prom_metric_file'] = '/somedir/omnibus_package.prom'
        end.converge(described_recipe)
      end

      it 'installs the gitlab-ee package with name and version' do
        expect(chef_run).to install_apt_package('gitlab-ee').with(
          version: "11.9.8-ee.0"
        )
      end

      it 'creates the metric file' do
        expect(chef_run).to create_file('/somedir/omnibus_package.prom').with(
          mode: '0644',
          content: 'omnibus_package_install{enable="true"} 1.0'
        )
      end
    end

    context 'when package install is not enabled' do
      before do
        allow(Dir).to receive(:exist?).with('/somedir').and_return(true)
      end

      let(:chef_run) do
        ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node, _server|
          node.normal['omnibus-gitlab']['package']['name'] = "gitlab-ee"
          node.normal['omnibus-gitlab']['package']['version'] = "11.9.8-ee.0"
          node.normal['omnibus-gitlab']['package']['enable'] = false
          node.normal['omnibus-gitlab']['package']['prom_metric_file'] = '/somedir/omnibus_package.prom'
        end.converge(described_recipe)
      end

      it 'does not install the gitlab-ee package with name and version' do
        expect(chef_run).not_to install_apt_package('gitlab-ee').with(
          version: "11.9.8-ee.0"
        )
      end

      it 'creates the metric file' do
        expect(chef_run).to create_file('/somedir/omnibus_package.prom').with(
          mode: '0644',
          content: 'omnibus_package_install{enable="false"} 1.0'
        )
      end
    end

    context 'when `skip_auto_reconfigure` is set' do
      it 'deletes the skip file' do
        expect(chef_run).to delete_file('/etc/gitlab/skip-auto-reconfigure')
      end
    end
  end
end
